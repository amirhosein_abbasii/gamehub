from django.contrib import admin
from .models import Tournament


class TournamentAdmin(admin.ModelAdmin):
    list_display = ('title', 'code', 'capacity', 'price', 'start_date', 'registeration_date', 'is_published')
    list_display_links = ('title', 'code')
    list_filter = ('title', 'code', 'capacity', 'start_date', 'registeration_date')
    search_fields = ('title', 'code', 'capacity', 'start_date', 'registeration_date')
    list_editable = ('is_published',)
    list_per_page = 25


admin.site.register(Tournament, TournamentAdmin)