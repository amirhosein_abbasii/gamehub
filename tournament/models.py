from django.db import models
from datetime import datetime


class Tournament(models.Model):
    title = models.CharField(max_length=100, default='---')
    category = models.CharField(max_length=100, default='---')
    code = models.CharField(max_length=100,default='0000')
    capacity = models.CharField(max_length=100,default='1000')
    price = models.CharField(max_length=100,default='0$')
    start_date = models.DateTimeField(default=datetime.now, blank=True)
    registeration_date = models.DateTimeField(default=datetime.now, blank=True)
    is_published = models.BooleanField(default=False)
    

    def __str__(self):
        return self.title