from django.shortcuts import render, redirect
from django.http import HttpResponse
from . import forms

def register(request):
    return render(request, 'pages/register.html')


def viewers(request):
    return HttpResponse('<h1>viewers</h1>')

def post(request):
    if request.method == 'POST':
        form = forms.RegisterForm(request.POST, request.FILES)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.is_published = False
            instance.save()
            # messages.success(request, 'Your message has been sent ,we will get back to you soon,Thank you')
            return redirect('register')
    else:
        form = forms.UploadForm()
   
    return render(request, 'pages/register.html', {'form': form})

def dashboard(request):
    return render(request, 'pages/dashboard.html')