# Generated by Django 3.2.4 on 2021-07-01 08:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0004_alter_player_date_register'),
    ]

    operations = [
        migrations.AlterField(
            model_name='player',
            name='email',
            field=models.CharField(default='', max_length=150),
        ),
        migrations.AlterField(
            model_name='player',
            name='fname',
            field=models.CharField(default='', max_length=150, verbose_name='Full name'),
        ),
        migrations.AlterField(
            model_name='player',
            name='tel',
            field=models.CharField(default='', max_length=150, verbose_name='telephone'),
        ),
        migrations.AlterField(
            model_name='player',
            name='username',
            field=models.CharField(default='', max_length=150),
        ),
    ]
