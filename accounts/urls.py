from django.urls import path, include
from . import views


urlpatterns = [
   path('register', views.register, name='register'),
   path('viewers', views.viewers, name='viewers'),
   path('post', views.post, name='reg'),
   path('dashboard', views.dashboard, name='dashboard'),
]