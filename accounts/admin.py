from django.contrib import admin
from .models import Player

class PlayersAdmin(admin.ModelAdmin):
    list_display = ('fname', 'username', 'email', 'credit', 'date_register',)
    list_display_links = ('fname', 'username', 'email',)
    list_filter = ('date_register',)
    search_fields = ('fname', 'username', 'email', 'date_register',)

admin.site.register(Player, PlayersAdmin)
