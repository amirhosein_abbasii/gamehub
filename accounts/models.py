from django.db import models
from datetime import datetime
class Player(models.Model):
    fname = models.CharField(max_length=150, verbose_name='Full name', default='')
    username = models.CharField(max_length=150, default='')
    email = models.CharField(max_length=150, default='')
    tel = models.CharField(max_length=150, verbose_name='telephone', default='')
    credit = models.IntegerField(default=0, help_text='$')
    acc = models.TextField(default='---')
    date_register = models.DateField(default=datetime.now)
    def __str__(self):
        return self.fname
