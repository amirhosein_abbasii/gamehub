from django import forms
from .models import Player
from datetime import datetime


class RegisterForm(forms.ModelForm):
  class Meta:
    model = Player
    fields = ['fname', 'username', 'email', 'tel'
               ]