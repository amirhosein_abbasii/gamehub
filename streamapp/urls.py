from django.urls import path, include
from streamapp import views


urlpatterns = [
    path('', views.indexx, name='indexx'),
    path('video_feed', views.video_feed, name='video_feed'),
    path('webcam_feed', views.webcam_feed, name='webcam_feed'),
    ]
