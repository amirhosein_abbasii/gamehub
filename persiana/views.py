from django.shortcuts import render
from django.http import HttpResponse
from . models import Customer

def customer(request):
    return HttpResponse('<h1>customer</h1>')

def pannel(request):
    information = Customer.objects.filter(is_published=True)
    context = {
        'information': information, 
    }

    return render(request,'pages/pannel.html', context)