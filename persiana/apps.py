from django.apps import AppConfig


class PersianaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'persiana'
