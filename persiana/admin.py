from django.contrib import admin
from . models import Customer

class CustomerAdmin(admin.ModelAdmin):
    list_display = ('fname', 'tel', 'email', 'contract', 'start_date', 'expire_date', 'publish_date', 'is_published')
    list_display_links = ('fname', 'tel', 'email', 'contract', 'start_date', 'expire_date')
    list_filter = ('fname', 'publish_date', 'is_published')
    list_editable = ('is_published',)
    search_fields = ('fname', 'tel', 'email', 'start_date', 'expire_date', 'publish_date')
    list_per_page = 25

admin.site.register(Customer, CustomerAdmin)