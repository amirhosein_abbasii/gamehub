from django.db import models
from datetime import datetime


class Customer(models.Model):
    fname = models.CharField(max_length=150, verbose_name='Company')
    email = models.EmailField(max_length=200)
    tel = models.CharField(max_length=20)
    address = models.TextField(max_length=10000)
    schedule = models.TextField(max_length=10000, default='family')
    contract = models.FileField(upload_to='photos/%Y/%m/%d/')
    start_date = models.DateTimeField(default=datetime.now)
    expire_date = models.DateTimeField(default=datetime.now)
    payment_date = models.DateTimeField(default=datetime.now)
    publish_date = models.DateTimeField(default=datetime.now)
    is_published = models.BooleanField(default=False)
    credit = models.CharField(max_length=100, default='$')
    def __str__(self): return self.fname