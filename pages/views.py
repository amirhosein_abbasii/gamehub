from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Game, Video, News, Message, About, SiteVideos
from tournament.models import Tournament
from . import forms
from django.contrib.auth.decorators import login_required
from django.contrib import messages
import requests


def index(request):
    games = Game.objects.filter(is_published=True)
    videos = Video.objects.filter(is_published=True)
    context = {
        'games': games, 
        'videos' : videos,
    }
    return render(request, 'pages/index.html', context)

def blog(request):
    return HttpResponse('<h1>blog</h1>')



def about_us(request):
    about = About.objects.filter(is_published=True)
    context = {
        'about': about,
    }
    return render(request, 'pages/about.html', context)


def news(request):
    news = News.objects.filter(is_published=True)
    context = {
        'news': news,       
    }
    
    return render(request, 'pages/news.html', context)




def post(request):
    if request.method == 'POST':
        form = forms.UploadForm(request.POST, request.FILES)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.is_published = False
            instance.save()
            # messages.success(request, 'Your message has been sent ,we will get back to you soon,Thank you')
            return redirect('index')
    else:
        form = forms.UploadForm()
   
    return render(request, 'pages/index.html', {'form': form})

def dashboard(request):
    tournaments = Tournament.objects.filter(is_published=True)
    context ={
        'tournaments': tournaments,
    }

    return render(request, 'pages/dashboard.html', context)


def mystream(request):
    videos = SiteVideos.objects.filter(is_published=True)
    context ={
        'videos': videos,
    }

    return render(request, 'pages/stream.html', context)

def mylive(request):
    return render(request, 'pages/mylive.html')