# Generated by Django 3.2.4 on 2021-06-29 09:33

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0004_video_is_published'),
    ]

    operations = [
        migrations.AddField(
            model_name='game',
            name='publish_date',
            field=models.DateTimeField(blank=True, default=datetime.datetime.now),
        ),
    ]
