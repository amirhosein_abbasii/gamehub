# Generated by Django 3.2.4 on 2021-06-29 08:13

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Game',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fname', models.CharField(max_length=50, verbose_name='Full name')),
                ('shname', models.CharField(max_length=50, verbose_name='Short name')),
                ('photo', models.ImageField(upload_to='photos/%Y/%m/%d/')),
            ],
        ),
    ]
