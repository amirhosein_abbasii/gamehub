# Generated by Django 3.2.4 on 2021-06-30 11:00

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0012_aboutype'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='aboutype',
            new_name='About',
        ),
    ]
