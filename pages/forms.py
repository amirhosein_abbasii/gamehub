from django import forms
from .models import Message
from datetime import datetime


class UploadForm(forms.ModelForm):
  class Meta:
    model = Message
    fields = ['name', 'email', 'message'
               ]


