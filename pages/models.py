from django.db import models
from datetime import datetime


class Game(models.Model):
    fname = models.CharField(max_length=50, verbose_name='Full name')
    shname = models.CharField(max_length=50, verbose_name='Short name')
    photo = models.ImageField(upload_to='photos/%Y/%m/%d/')
    is_published = models.BooleanField(default=False)
    publish_date = models.DateTimeField(default=datetime.now, blank=True)
    
    def __str__(self):
        return self.fname

class Video(models.Model):
    videos = models.FileField(upload_to='photos/%Y/%m/%d/')
    title = models.CharField(max_length=100, default='my video')
    is_published = models.BooleanField(default=False)


class News(models.Model):
    
    class Meta:
        verbose_name = ("news")
        verbose_name_plural = ("news")
    author = models.CharField(max_length=100, default='---')
    category = models.CharField(max_length=100, default='---')
    title = models.CharField(max_length=100)
    body = models.TextField(max_length=10000)
    tags1 = models.CharField(max_length=100, default='', blank=True)
    tags2 = models.CharField(max_length=100, default='', blank=True)
    tags3 = models.CharField(max_length=100, default='', blank=True)
    tags4 = models.CharField(max_length=100, default='', blank=True)
    tags5 = models.CharField(max_length=100, default='', blank=True)
    photo = models.ImageField(upload_to='photos/%Y/%m/%d/')
    is_published = models.BooleanField(default=False)
    publish_date = models.DateTimeField(default=datetime.now, blank=True)

    def __str__(self):
        return self.title

class Message(models.Model):
    name = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    message = models.TextField(max_length=500)
    message_date = models.DateTimeField(default=datetime.now, blank=True)

    def __str__(self):
        return self.name


class About(models.Model):
    title = models.CharField(max_length=100)
    body = models.TextField(max_length=10000)
    is_published = models.BooleanField(default=False)
    publish_date = models.DateTimeField(default=datetime.now, blank=True)
    
    def __str__(self):
        return self.title


class SiteVideos(models.Model):
    video = models.FileField(upload_to='photos/%Y/%m/%d/')
    title = models.CharField(max_length=100, default='my video') 
    thumb = models.FileField(upload_to='photos/%Y/%m/%d/', blank=True)
    is_published = models.BooleanField(default=False)

    def __str__(self):
        return self.title