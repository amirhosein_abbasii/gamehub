from django.contrib import admin
from . models import Game, Video, News, Message, About, SiteVideos

class GameAdmin(admin.ModelAdmin):
    list_display = ('id', 'fname', 'shname', 'publish_date', 'is_published')
    list_display_links = ('id', 'fname', 'shname')
    list_filter = ('publish_date', 'is_published')
    list_editable = ('is_published',)
    search_fields = ('fname', 'shname', 'publish_date', 'is_published')
    list_per_page = 25


class NewsAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'category', 'author', 'publish_date', 'is_published')
    list_display_links = ('title', 'category', 'author', 'publish_date')
    list_filter = ('publish_date', 'is_published', 'category', 'author')
    list_editable = ('is_published',)
    search_fields = ('title', 'category', 'author', 'publish_date')
    list_per_page = 25

class MessageAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'message_date')
    list_display_links = ('name', 'email', 'message_date')
    list_filter = ('message_date',)
    search_fields = (('name', 'email', 'message_date'))
    list_per_page = 25

class AboutAdmin(admin.ModelAdmin):
    list_display = ('title', 'publish_date', 'is_published')
    list_display_links = ('title', 'publish_date')
    list_filter = ('title', 'publish_date')
    search_fields = ('title', 'publish_date')
    list_editable = ('is_published',)
    list_per_page = 25


class SiteVideosAdmin(admin.ModelAdmin):
    list_display = ('title','is_published')
    list_display_links = ('title',)
    list_editable = ('is_published',)
    search_fields = ('title',)
    list_per_page = 25





admin.site.register(Game, GameAdmin)
admin.site.register(News, NewsAdmin)
admin.site.register(Message, MessageAdmin)
admin.site.register(About, AboutAdmin)
admin.site.register(SiteVideos, SiteVideosAdmin)
admin.site.register(Video)
