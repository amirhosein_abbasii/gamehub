from django.urls import path
from . import views

urlpatterns = [
   path('', views.index, name='index'),
   path('about', views.about_us, name='about'),
   path('blog', views.blog, name='blog'),
   path('news', views.news, name='news'),
   path('message', views.post, name='message'),
   path('dashboard', views.dashboard, name='dashboard'),
   path('mystream', views.mystream, name='mystream'),
   path('mylive', views.mylive, name='mylive'),
]